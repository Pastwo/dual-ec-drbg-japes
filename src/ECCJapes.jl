module ECCJapes

import Base: +, -, *, ^, /, ==, inv, sqrt, show, div
export EllipticCurve, ECPoint, ModularField


include("ModularArithmetic.jl")

function bingis(x,y)
    return x+y
end

struct EllipticCurve
    a::Integer
    b::Integer
    p::Integer
end

struct ECPoint
    x::ModularField
    y::ModularField
    curve::EllipticCurve
end

ECPoint(x::Integer, y::Integer, curve::EllipticCurve) = ECPoint(ModularField(x,curve.p), ModularField(y,curve.p), curve)

function show(io::IO, curve::EllipticCurve)

    print(io, "This is an elliptic curve - should make this a plot")
    # plt = plot_curve(curve)
    # png(plt, io)
end

function +(P::ECPoint,Q::ECPoint)
    if P.curve != Q.curve
        print("Not on the same curve")
        throw(DomainError("Points do not exist on the same curve"))
    end

    if P == Q   # Same point - just do doubling
        λ = (3*P.x^2 + P.curve.a) / (2*P.y)
        x = λ^2 - P.x - Q.x
        y = λ * (P.x - x) - P.y
        return ECPoint(x,y,P.curve)
    elseif P.x != Q.x
        λ = (Q.y - P.y) ÷ (Q.x - P.x)
        x = λ^2 - P.x - Q.x
        y = λ * (P.x - x) - P.y
        return ECPoint(x,y,P.curve)
    else
        throw(DomainError("What in the sam hell?"))
    end
end

function plot_curve(curve::EllipticCurve)
    # Add the actual plotting
    points = [(i, curve_y(curve, i)) for i in 1:100]
    x = [p[1] for p in points]
    y = [p[2] for p in points]
    return plot(x,y, fmt= :png)
end

function curve_y(curve::EllipticCurve, x::Integer)
    y² = x^3 + curve.a*x + curve.b
    return sqrt(y²)
end
    
end
