"Acts like a normal integer but performs it's operations using modular arithmitec"
struct ModularField <: Integer
    n::Integer
    p::Integer
end

function show(io::IO, z::ModularField)
    print(io, z.n, " mod ", z.p)
end

"Adds two numbers using modular arithmitec"
function +(X₁::ModularField,X₂::ModularField)
    if X₁.p != X₂.p
        throw(DomainError("Two numbers are using differet modulus"))
    else
        n = mod(X₁.n + X₂.n, X₁.p)
        return typeof(X₁)(n, X₁.p)
    end
end

"Adds one modular number and one normal int using modular arithmitec"
function +(X₁::ModularField,c::Integer)
    n = mod(X₁.n + c, X₁.p)
    return typeof(X₁)(n, X₁.p)
end

"Subtract two numbers using modular arithmitec"
function -(X₁::ModularField,X₂::ModularField)
    if X₁.p != X₂.p
        throw(DomainError("Two numbers are using differet modulus"))
    else
        n = mod(X₁.n - X₂.n, X₁.p)
        return typeof(X₁)(n, X₁.p)
    end
end

"Multiplies two numbers using modular arithmitec"
function *(X₁::ModularField,X₂::ModularField)
    if X₁.p != X₂.p
        throw(DomainError("Two numbers are using differet modulus"))
    else
        n = mod(X₁.n * X₂.n, X₁.p)
        return typeof(X₁)(n, X₁.p)
    end
end

"Multiplies a modular field number and a normal integer"
function *(c::Integer, X₁::ModularField)
    n = mod(X₁.n * c, X₁.p)
    return typeof(X₁)(n, X₁.p)
end

"Returns Xᵏ using Fermat's Little Theorem"
function ^(X₁::ModularField,k::Int)
    n = powermod(X₁.n, mod(k, (X₁.p - 1)), X₁.p)
    return typeof(X₁)(n, X₁.p)
end

"Returns 1/X as a special case of exponentiation where k = -1"
function inv(X₁::ModularField)
    n = powermod(X₁.n, mod(-1, (X₁.p - 1)), X₁.p)
    return typeof(X₁)(n, X₁.p)
end

"Returns 𝑋₁/𝑋₂ using Fermat's Little Theorem"
function /(X₁::ModularField,X₂::ModularField)
    if X₁.p != X₂.p
        throw(DomainError("Two numbers are using differet modulus"))
    else
        n = mod(X₁.n * powermod(X₂.n, X₁.p - 2, X₁.p), X₁.p)
        return typeof(X₁)(n, X₁.p)
    end
end

function div(X₁::ModularField,X₂::ModularField)
    return X₁ / X₂
end