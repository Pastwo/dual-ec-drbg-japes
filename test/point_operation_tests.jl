using Test

@testset "dinkis" begin
    curve = ECCJapes.EllipticCurve(1,1,1)
    point = ECCJapes.ECPoint(1,1,curve)
    @test true
end

@testset "Point Equality" begin
    x = 5
    y = 9
    a = 11
    b = 13
    p = 16
    curve = EllipticCurve(a,b,p)
    point1 = (x,y,curve)
    point2 = (x,y,curve)
    @test point1 == point2
end

@testset "Point Addition" begin
    a = 1
    b = 3
    p = 97
    test_curve = ECCJapes.EllipticCurve(a,b,p)
    test_points = [ECCJapes.ECPoint(70,9,test_curve), ECCJapes.ECPoint(70,9,test_curve)]
    test_result = ECCJapes.ECPoint(63,12,test_curve)
    @test sum(test_points) == test_result
    
end