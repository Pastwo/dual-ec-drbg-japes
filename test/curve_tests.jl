using Test

@testset "Curve Equality" begin
    curve1 = EllipticCurve(1,2,3)
    curve2 = EllipticCurve(1,2,3)
    @test curve1 == curve2
end