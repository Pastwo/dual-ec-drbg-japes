using Test

@testset "Addition" begin
    p = 7
    x1 = ModularField(1, p)
    x2 = ModularField(3, p)
    x3 = ModularField(5, p)
    @test x1 + x2 == ModularField(4, p)
    @test x1 + x3 == ModularField(6, p)
    @test x2 + x3 == ModularField(1, p)
end

@testset "Subtraction" begin
    p = 10
    x1 = ModularField(1, p)
    x2 = ModularField(-4, p)
    x3 = ModularField(8, p)
    @test x1 - x2 == ModularField(5, p)
    @test x1 - x3 == ModularField(3, p)
    @test x2 - x3 == ModularField(8, p)
end

@testset "Multiplication" begin
    p = 5
    x1 = ModularField(1, p)
    x2 = ModularField(4, p)
    x3 = ModularField(6, p)
    @test x1 * x2 == ModularField(4, p)
    @test x1 * x3 == ModularField(1, p)
    @test x2 * x3 == ModularField(4, p)
end

@testset "Power" begin
    p = 31
    x1 = ModularField(17, p)
    x2 = ModularField(5, p)
    x3 = ModularField(18, p)
    @test x1^3 == ModularField(15,p)
    @test x2^5 * x3 == ModularField(16, p)
end

@testset "Division" begin
    p = 31
    x1 = ModularField(3, p)
    x2 = ModularField(24, p)
    x3 = ModularField(17, p)
    x4 = ModularField(4, p)
    x5 = ModularField(11, p)
    @test x1/x2 == ModularField(4, p)
    @test x3^-3 == ModularField(29, p)
    @test x4^-4*x5 == ModularField(13, p)
end