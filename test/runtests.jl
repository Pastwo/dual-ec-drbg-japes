using ECCJapes
using Test

@testset "All Tests" begin
    @testset "Basic Tests" begin
        include("basic_tests.jl")
    end
    @testset "Point Operations Tests" begin
        include("point_operation_tests.jl")
    end

    @testset "Curve Tests" begin
        include("curve_tests.jl")
    end

    @testset "Modular Arithmetic Tests" begin
        include("modular_arithmetic_tests.jl")
    end
end