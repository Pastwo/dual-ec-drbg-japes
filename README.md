# Dual EC DRBG Japes


This is just a little project implementing an (alleged) NSA backdoor into an old (now retracted) NIST cryptographic standard.

For more information:   https://en.wikipedia.org/wiki/Dual_EC_DRBG 

Obviously no malicious intent, I just find the history of the backdoor fascinating and the actual math is simple enough for a novice like myself to implement.
